---
title: Home
nav_order: 1
---

[fp-ts](https://github.com/gcanti/fp-ts) bindings for rxjs

# Implemented instances

- `Monad`
- `Alternative`
- `Filterable`

# Example

```ts
import { from } from 'rxjs'
import { pipe } from 'fp-ts/function'
import * as OB from 'fp-ts-rxjs/Observable'

const fa = from([1, 2, 3])
const fb = pipe(
  fa,
  OB.chain(fa, (a) => from([a, a + 1]))
)
// fb will emit 1, 2, 2, 3, 3, 4
```

# TypeScript compatibility

The stable version is tested against TypeScript 4.2.4

# rxjs compatibility

| rxjs version | @gvrs/fp-ts-rxjs version |
| ------------ | ------------------------ |
| `rxjs@7`     | `@gvrs/fp-ts-rxjs@0.7.x` |
