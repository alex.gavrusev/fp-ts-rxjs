/**
 * Lift a computation from the `Observable` monad
 *
 * @since 0.6.6
 */
import { HKT, URIS, URIS2, URIS3, URIS4 } from 'fp-ts/HKT'
import {
  NaturalTransformation11,
  NaturalTransformation12,
  NaturalTransformation12C,
  NaturalTransformation13,
  NaturalTransformation13C,
  NaturalTransformation14,
} from 'fp-ts/NaturalTransformation'
import { MonadTask, MonadTask1, MonadTask2, MonadTask2C, MonadTask3, MonadTask3C, MonadTask4 } from 'fp-ts/MonadTask'
import { Observable } from 'rxjs'

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable<M> extends MonadTask<M> {
  readonly fromObservable: <A>(fa: Observable<A>) => HKT<M, A>
}

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable1<M extends URIS> extends MonadTask1<M> {
  readonly fromObservable: NaturalTransformation11<'Observable', M>
}

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable2<M extends URIS2> extends MonadTask2<M> {
  readonly fromObservable: NaturalTransformation12<'Observable', M>
}

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable2C<M extends URIS2, E> extends MonadTask2C<M, E> {
  readonly fromObservable: NaturalTransformation12C<'Observable', M, E>
}

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable3<M extends URIS3> extends MonadTask3<M> {
  readonly fromObservable: NaturalTransformation13<'Observable', M>
}

/**
 * @category type classes
 * @since 0.6.6
 */
export interface MonadObservable3C<M extends URIS3, E> extends MonadTask3C<M, E> {
  readonly fromObservable: NaturalTransformation13C<'Observable', M, E>
}

/**
 * @category type classes
 * @since 0.6.7
 */
export interface MonadObservable4<M extends URIS4> extends MonadTask4<M> {
  readonly fromObservable: NaturalTransformation14<'Observable', M>
}
