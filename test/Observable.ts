import * as assert from 'assert'
import { from } from 'rxjs'
import * as O from 'fp-ts/Option'
import * as E from 'fp-ts/Either'
import * as T from 'fp-ts/Task'
import { identity, pipe } from 'fp-ts/function'

import * as _ from '../src/Observable'

import { bufferOB } from './test.helpers'

describe('Observable', () => {
  it('of', async () => {
    const fa = _.of(1)
    const events = await pipe(fa, bufferOB)

    assert.deepStrictEqual(events, [1])
  })

  it('map', async () => {
    const fa = from([1, 2, 3])
    const double = (n: number): number => n * 2
    const fb = pipe(fa, _.map(double))
    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [2, 4, 6])
  })

  it('ap', async () => {
    const fa = from([1, 2, 3])
    const double = (n: number): number => n * 2
    const triple = (n: number): number => n * 3
    const fab = from([double, triple])
    const fb = pipe(fab, _.ap(fa))
    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [3, 6, 9])
  })

  it('apFirst', async () => {
    const events = await pipe(pipe(from([1]), _.apFirst(from([2]))), bufferOB)

    assert.deepStrictEqual(events, [1])
  })

  it('apSecond', async () => {
    const events = await pipe(pipe(from([1]), _.apSecond(from([2]))), bufferOB)

    assert.deepStrictEqual(events, [2])
  })

  it('chain', async () => {
    const fa = from([1, 2, 3])
    const fb = pipe(
      fa,
      _.chain((a) => from([a, a + 1]))
    )

    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [1, 2, 2, 3, 3, 4])
  })

  it('chainFirst', async () => {
    const fa = from([1, 2, 3])
    const fb = pipe(
      fa,
      _.chainFirst((a) => from([a, a + 1]))
    )

    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [1, 1, 2, 2, 3, 3])
  })

  it('filterMap', async () => {
    const fa = from([1, 2, 3])
    const fb = pipe(fa, _.filterMap(O.fromPredicate((n) => n > 1)))

    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [2, 3])
  })

  it('compact', async () => {
    const fa = from([1, 2, 3].map(O.fromPredicate((n) => n > 1)))
    const fb = _.compact(fa)

    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [2, 3])
  })

  it('filter', async () => {
    const fa = from([1, 2, 3])
    const fb = pipe(
      fa,
      _.filter((n) => n > 1)
    )

    const events = await pipe(fb, bufferOB)

    assert.deepStrictEqual(events, [2, 3])
  })

  it('partitionMap', async () => {
    const fa = from([1, 2, 3])
    const s = pipe(fa, _.partitionMap(E.fromPredicate((n) => n > 1, identity)))

    const e2 = await pipe(s.left, bufferOB)

    assert.deepStrictEqual(e2, [1])

    const e1 = await pipe(s.right, bufferOB)

    assert.deepStrictEqual(e1, [2, 3])
  })

  it('separate', async () => {
    const fa = from([1, 2, 3].map(E.fromPredicate((n) => n > 1, identity)))
    const s = _.separate(fa)

    const e2 = await pipe(s.left, bufferOB)

    assert.deepStrictEqual(e2, [1])

    const e1 = await pipe(s.right, bufferOB)

    assert.deepStrictEqual(e1, [2, 3])
  })

  it('partition', async () => {
    const fa = from([1, 2, 3])
    const s = pipe(
      fa,
      _.partition((n) => n > 1)
    )

    const e2 = await pipe(s.left, bufferOB)

    assert.deepStrictEqual(e2, [1])

    const e1 = await pipe(s.right, bufferOB)

    assert.deepStrictEqual(e1, [2, 3])
  })

  it('zero', async () => {
    const events = await pipe(_.zero(), bufferOB)
    assert.deepStrictEqual(events, [])
  })

  it('alt', async () => {
    const events = await pipe(
      pipe(
        _.of(1),
        _.alt(() => _.of(2))
      ),
      bufferOB
    )

    assert.deepStrictEqual(events, [1, 2])
  })

  it('getMonoid', async () => {
    const M = _.getMonoid<number>()
    const events = await pipe(M.concat(_.of(1), _.of(2)), bufferOB)

    assert.deepStrictEqual(events, [1, 2])
  })

  it('fromOption', async () => {
    const events = await pipe(_.fromOption(O.some(1)), bufferOB)
    assert.deepStrictEqual(events, [1])

    const noEvents = await pipe(_.fromOption(O.none), bufferOB)
    assert.deepStrictEqual(noEvents, [])
  })

  it('fromIO', async () => {
    const events = await pipe(
      _.fromIO(() => 1),
      bufferOB
    )

    assert.deepStrictEqual(events, [1])
  })

  it('fromTask', async () => {
    const events = await pipe(_.fromTask(T.of(1)), bufferOB)

    assert.deepStrictEqual(events, [1])
  })

  it('toTask', async () => {
    const t = await _.toTask(_.of(1))()

    assert.deepStrictEqual(t, 1)
  })

  it('toTaskOption', async () => {
    const t1 = await _.toTaskOption(_.of(1))()
    assert.deepStrictEqual(t1, O.some(1))

    const t2 = await _.toTaskOption(_.of(undefined))()
    assert.deepStrictEqual(t2, O.some(undefined))

    const t3 = await _.toTaskOption(_.zero())()
    assert.deepStrictEqual(t3, O.none)
  })

  it('do notation', async () => {
    const t = await pipe(
      pipe(
        _.of(1),
        _.bindTo('a'),
        _.bind('b', () => _.of('b'))
      ),
      bufferOB
    )

    assert.deepStrictEqual(t, [{ a: 1, b: 'b' }])
  })
})
