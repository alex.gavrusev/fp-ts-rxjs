import * as assert from 'assert'
import * as TE from 'fp-ts/TaskEither'
import * as T from 'fp-ts/Task'
import * as E from 'fp-ts/Either'
import * as IO from 'fp-ts/IO'
import { pipe } from 'fp-ts/function'
import * as O from 'fp-ts/Option'
import { of as rxOf, Observable, throwError as rxThrowError } from 'rxjs'

import * as _ from '../src/ObservableEither'

import { bufferOB } from './test.helpers'

describe('ObservableEither', () => {
  it('rightIO', async () => {
    const e = await pipe(_.rightIO(IO.of(1)), bufferOB)
    assert.deepStrictEqual(e, [E.right(1)])
  })
  it('leftIO', async () => {
    const e = await pipe(_.leftIO(IO.of(1)), bufferOB)
    assert.deepStrictEqual(e, [E.left(1)])
  })

  it('fromTaskEither', async () => {
    const e = await pipe(_.fromTaskEither(TE.right(1)), bufferOB)
    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('toTaskEither', async () => {
    const e = await _.toTaskEither(_.right(1))()
    assert.deepStrictEqual(e, E.right(1))
  })

  it('fromTask', async () => {
    const e = await pipe(_.fromTask(T.of(1)), bufferOB)
    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('tryCatch', async () => {
    const e1 = await pipe(pipe(rxOf(1), _.tryCatch), bufferOB)
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      pipe(
        rxThrowError(() => new Error('Uncaught Error')),
        _.tryCatch,
        _.mapLeft(() => 'Caught Error')
      ),
      bufferOB
    )
    assert.deepStrictEqual(e2, [E.left('Caught Error')])
  })

  it('fold left', async () => {
    const f = (n: number): Observable<number> => rxOf(n * 2)
    const g = (n: number): Observable<number> => rxOf(n * 3)
    const e = await pipe(pipe(_.left(2), _.fold(f, g)), bufferOB)
    assert.deepStrictEqual(e, [4])
  })

  it('fold right', async () => {
    const f = (n: number): Observable<number> => rxOf(n * 2)
    const g = (n: number): Observable<number> => rxOf(n * 3)
    const e = await pipe(pipe(_.right(3), _.fold(f, g)), bufferOB)
    assert.deepStrictEqual(e, [9])
  })

  it('getOrElse (left)', async () => {
    const onLeft = (s: string): Observable<number> => rxOf(s.length)
    const e = await pipe(pipe(_.left('four'), _.getOrElse(onLeft)), bufferOB)
    assert.deepStrictEqual(e, [4])
  })

  it('getOrElse (right)', async () => {
    const onLeft = (s: string): Observable<number> => rxOf(s.length)
    const e = await pipe(pipe(_.right(1), _.getOrElse(onLeft)), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('orElse (left)', async () => {
    const onLeft = (s: string): _.ObservableEither<number, number> => _.left(s.length)
    const e = await pipe(pipe(_.left('four'), _.orElse(onLeft)), bufferOB)
    assert.deepStrictEqual(e, [E.left(4)])
  })

  it('orElse (right)', async () => {
    const onLeft = (s: string): _.ObservableEither<number, number> => _.left(s.length)
    const e = await pipe(pipe(_.right(1), _.orElse(onLeft)), bufferOB)
    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('swap left to right', async () => {
    const e = await pipe(pipe(_.left(1), _.swap), bufferOB)
    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('swap right to left', async () => {
    const e = await pipe(pipe(_.right(1), _.swap), bufferOB)
    assert.deepStrictEqual(e, [E.left(1)])
  })

  describe('Monad', () => {
    it('of', async () => {
      const fea = _.of(1)
      const x = await pipe(fea, bufferOB)
      assert.deepStrictEqual(x, [E.right(1)])
    })

    it('map', async () => {
      const double = (n: number): number => n * 2
      const x = await pipe(pipe(_.right(1), _.map(double)), bufferOB)
      assert.deepStrictEqual(x, [E.right(2)])
    })

    it('ap', async () => {
      const double = (n: number): number => n * 2
      const mab = _.right(double)
      const ma = _.right(1)
      const x = await pipe(pipe(mab, _.ap(ma)), bufferOB)
      assert.deepStrictEqual(x, [E.right(2)])
    })

    it('chain', async () => {
      const f = (a: string): _.ObservableEither<string, number> => (a.length > 2 ? _.right(a.length) : _.left('text'))
      const e1 = await pipe(pipe(_.right('four'), _.chain(f)), bufferOB)
      assert.deepStrictEqual(e1, [E.right(4)])

      const e2 = await pipe(pipe(_.right('a'), _.chain(f)), bufferOB)
      assert.deepStrictEqual(e2, [E.left('text')])

      const e3 = await pipe(pipe(_.left('b'), _.chain(f)), bufferOB)
      assert.deepStrictEqual(e3, [E.left('b')])
    })

    it('left identity', async () => {
      const f = (a: string): _.ObservableEither<string, number> => (a.length > 2 ? _.right(a.length) : _.left('text'))
      const a = 'text'
      const e1 = await pipe(_.of<string, string>(a), _.chain(f), bufferOB)
      const e2 = await pipe(f(a), bufferOB)
      assert.deepStrictEqual(e1, e2)
    })

    it('right identity', async () => {
      const fa = _.of(1)
      const e1 = await pipe(pipe(fa, _.chain(_.of)), bufferOB)
      const e2 = await pipe(fa, bufferOB)
      assert.deepStrictEqual(e1, e2)
    })
  })

  it('apFirst', async () => {
    const e = await pipe(pipe(_.right(1), _.apFirst(_.right(2))), bufferOB)

    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('apSecond', async () => {
    const e = await pipe(pipe(_.right(1), _.apSecond(_.right(2))), bufferOB)

    assert.deepStrictEqual(e, [E.right(2)])
  })

  it('chainFirst', async () => {
    const f = (a: string): _.ObservableEither<string, number> => (a.length > 2 ? _.right(a.length) : _.left('b'))
    const e1 = await pipe(pipe(_.right('aaaa'), _.chainFirst(f)), bufferOB)
    assert.deepStrictEqual(e1, [E.right('aaaa')])
  })

  describe('Bifunctor', () => {
    it('bimap', async () => {
      const f = (s: string): number => s.length
      const g = (n: number): boolean => n > 2

      const e1 = await pipe(pipe(_.right(1), _.bimap(f, g)), bufferOB)
      assert.deepStrictEqual(e1, [E.right(false)])
      const e2 = await pipe(pipe(_.left('foo'), _.bimap(f, g)), bufferOB)
      assert.deepStrictEqual(e2, [E.left(3)])
    })

    it('mapLeft', async () => {
      const double = (n: number): number => n * 2
      const e = await pipe(pipe(_.left(1), _.mapLeft(double)), bufferOB)
      assert.deepStrictEqual(e, [E.left(2)])
    })
  })

  describe('Alt', () => {
    it('alt right right', async () => {
      const fx = _.right(1)
      const fy = () => _.right(2)
      const e1 = await pipe(pipe(fx, _.alt(fy)), bufferOB)
      assert.deepStrictEqual(e1, [E.right(1)])
    })

    it('alt left right', async () => {
      const fx = _.left<number, number>(1)
      const fy = () => _.right<number, number>(2)
      const e1 = await pipe(pipe(fx, _.alt(fy)), bufferOB)
      assert.deepStrictEqual(e1, [E.right(2)])
    })

    it('associativity', async () => {
      const fa = _.left<number, number>(1)
      const ga = () => _.right<number, number>(2)
      const ha = () => _.right<number, number>(3)

      const e1 = await pipe(pipe(pipe(fa, _.alt(ga)), _.alt(ha)), bufferOB)

      const e2 = await pipe(
        pipe(
          fa,
          _.alt(() => pipe(ga(), _.alt(ha))),
          bufferOB
        )
      )

      assert.deepStrictEqual(e1, e2)
    })

    it('distributivity', async () => {
      const double = (n: number): number => n * 2
      const fx = _.left<string, number>('left')
      const fy = () => _.right<string, number>(1)

      const e1 = await pipe(pipe(fx, _.alt(fy), _.map(double)), bufferOB)

      const e2 = await pipe(
        pipe(
          pipe(fx, _.map(double)),
          _.alt(() => pipe(fy(), _.map(double))),
          bufferOB
        )
      )

      assert.deepStrictEqual(e1, e2)
    })
  })

  it('do notation', async () => {
    const t = await pipe(
      pipe(
        _.right(1),
        _.bindTo('a'),
        _.bind('b', () => _.right('b'))
      ),
      bufferOB
    )

    assert.deepStrictEqual(t, [E.right({ a: 1, b: 'b' })])
  })

  it('fromOption', async () => {
    const e1 = await pipe(_.fromOption(() => 'a')(O.some(1)), bufferOB)
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(_.fromOption(() => 'a')(O.none), bufferOB)
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromEither', async () => {
    const e1 = await pipe(_.fromEither(E.right(1)), bufferOB)
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(_.fromEither(E.left('a')), bufferOB)
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('filterOrElse', async () => {
    const e1 = await pipe(
      _.filterOrElse(
        (n: number) => n > 0,
        () => 'a'
      )(_.of(1)),
      bufferOB
    )
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      _.filterOrElse(
        (n: number) => n > 0,
        () => 'a'
      )(_.of(-1)),
      bufferOB
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromPredicate', async () => {
    const e1 = await pipe(
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      )(1),
      bufferOB
    )
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      )(-1),
      bufferOB
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })
})
