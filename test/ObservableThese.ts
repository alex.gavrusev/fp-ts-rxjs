import * as assert from 'assert'
import * as TH from 'fp-ts/These'
import * as TT from 'fp-ts/TaskThese'
import * as T from 'fp-ts/Task'
import * as IO from 'fp-ts/IO'
import { pipe } from 'fp-ts/function'
import { Monoid } from 'fp-ts/string'
import { of as rxOf, Observable, lastValueFrom } from 'rxjs'

import * as _ from '../src/ObservableThese'

import { bufferOB } from './test.helpers'

describe('ObservableThese', () => {
  it('rightIO', async () => {
    const e = await pipe(_.rightIO(IO.of(1)), bufferOB)
    assert.deepStrictEqual(e, [TH.right(1)])
  })
  it('leftIO', async () => {
    const e = await pipe(_.leftIO(IO.of(1)), bufferOB)
    assert.deepStrictEqual(e, [TH.left(1)])
  })

  it('fromTaskThese', async () => {
    const e = await pipe(_.fromTaskThese(TT.right(1)), bufferOB)
    assert.deepStrictEqual(e, [TH.right(1)])
  })

  it('toTaskThese', async () => {
    const e = await _.toTaskThese(_.right(1))()
    assert.deepStrictEqual(e, TH.right(1))
  })

  it('fromTask', async () => {
    const e = await pipe(_.fromTask(T.of(1)), bufferOB)
    assert.deepStrictEqual(e, [TH.right(1)])
  })

  it('fold left', async () => {
    const f = (n: number): Observable<number> => rxOf(n * 2)
    const g = (n: number): Observable<number> => rxOf(n * 3)
    const n = (n: number, nn: number): Observable<number> => rxOf(n * nn * 4)
    const e = await pipe(pipe(_.left(2), _.fold(f, g, n)), bufferOB)
    assert.deepStrictEqual(e, [4])
  })

  it('fold right', async () => {
    const f = (n: number): Observable<number> => rxOf(n * 2)
    const g = (n: number): Observable<number> => rxOf(n * 3)
    const n = (n: number, nn: number): Observable<number> => rxOf(n * nn * 4)
    const e = await pipe(pipe(_.right(3), _.fold(f, g, n)), bufferOB)
    assert.deepStrictEqual(e, [9])
  })

  it('fold both', async () => {
    const f = (n: number): Observable<number> => rxOf(n * 2)
    const g = (n: number): Observable<number> => rxOf(n * 3)
    const n = (n: number, nn: number): Observable<number> => rxOf(n * nn * 4)
    const e = await pipe(pipe(_.both(5, 3), _.fold(f, g, n)), bufferOB)
    assert.deepStrictEqual(e, [60])
  })

  it('swap left to right', async () => {
    const e = await pipe(pipe(_.left(1), _.swap), bufferOB)
    assert.deepStrictEqual(e, [TH.right(1)])
  })

  it('swap right to left', async () => {
    const e = await pipe(pipe(_.right(1), _.swap), bufferOB)
    assert.deepStrictEqual(e, [TH.left(1)])
  })

  it('swap both to right and left', async () => {
    const e = await pipe(pipe(_.both(1, 2), _.swap), bufferOB)
    assert.deepStrictEqual(e, [TH.both(2, 1)])
  })

  it('map', async () => {
    const f = (n: number): number => n * 2

    const e1 = await pipe(pipe(_.right(1), _.map(f)), lastValueFrom)
    assert.deepStrictEqual(e1, TH.right(2))

    const e2 = await pipe(pipe(_.left('a'), _.map(f)), lastValueFrom)
    assert.deepStrictEqual(e2, TH.left('a'))

    const e3 = await pipe(pipe(_.both('a', 1), _.map(f)), lastValueFrom)
    assert.deepStrictEqual(e3, TH.both('a', 2))
  })

  it('bimap', async () => {
    const f = (s: string): string => s + '!'
    const g = (n: number): number => n * 2

    const e1 = await pipe(pipe(_.right(1), _.bimap(f, g)), lastValueFrom)
    assert.deepStrictEqual(e1, TH.right(2))

    const e2 = await pipe(pipe(_.left('a'), _.bimap(f, g)), lastValueFrom)
    assert.deepStrictEqual(e2, TH.left('a!'))

    const e3 = await pipe(pipe(_.both('a', 1), _.bimap(f, g)), lastValueFrom)
    assert.deepStrictEqual(e3, TH.both('a!', 2))
  })

  it('mapLeft', async () => {
    const f = (s: string): string => s + '!'

    const e1 = await pipe(pipe(_.right(1), _.mapLeft(f)), lastValueFrom)
    assert.deepStrictEqual(e1, TH.right(1))

    const e2 = await pipe(pipe(_.left('a'), _.mapLeft(f)), lastValueFrom)
    assert.deepStrictEqual(e2, TH.left('a!'))

    const e3 = await pipe(pipe(_.both('a', 1), _.mapLeft(f), lastValueFrom))
    assert.deepStrictEqual(e3, TH.both('a!', 1))
  })

  describe('getMonad', () => {
    const M = _.getMonad(Monoid)

    it('ap', async () => {
      const f = (n: number): number => n * 2

      const e = await pipe(M.ap(_.right(f), _.right(1)), lastValueFrom)

      assert.deepStrictEqual(e, TH.right(2))
    })

    it('chain', async () => {
      const f = (n: number) => (n > 2 ? _.both(`c`, n * 3) : n > 1 ? _.right(n * 2) : _.left(`b`))

      const e1 = await pipe(M.chain(_.right(1), f), lastValueFrom)
      assert.deepStrictEqual(e1, TH.left('b'))

      const e2 = await pipe(M.chain(_.right(2), f), lastValueFrom)
      assert.deepStrictEqual(e2, TH.right(4))

      const e3 = await pipe(M.chain(_.left('a'), f), lastValueFrom)
      assert.deepStrictEqual(e3, TH.left('a'))

      const e4 = await pipe(M.chain(_.both('a', 1), f), lastValueFrom)
      assert.deepStrictEqual(e4, TH.left('ab'))

      const e5 = await pipe(M.chain(_.both('a', 2), f), lastValueFrom)
      assert.deepStrictEqual(e5, TH.both('a', 4))

      const e6 = await pipe(M.chain(_.both('a', 3), f), lastValueFrom)
      assert.deepStrictEqual(e6, TH.both('ac', 9))
    })
  })
})
