import * as assert from 'assert'
import * as A from 'fp-ts/Array'
import { pipe, identity } from 'fp-ts/function'
import * as RE from 'fp-ts/Reader'
import * as T from 'fp-ts/Task'
import * as RT from 'fp-ts/ReaderTask'
import * as I from 'fp-ts/IO'
import * as O from 'fp-ts/Option'
import { from } from 'rxjs'
import * as E from 'fp-ts/Either'

import * as _ from '../src/ReaderObservable'
import * as R from '../src/Observable'

import { bufferOB } from './test.helpers'

describe('ReaderObservable', () => {
  describe('Monad', () => {
    it('map', async () => {
      const double = (n: number): number => n * 2

      const x = await pipe(pipe(_.of(1), _.map(double))({}), bufferOB)
      assert.deepStrictEqual(x, [2])
    })

    it('ap', async () => {
      const double = (n: number): number => n * 2
      const mab = _.of(double)
      const ma = _.of(1)

      const x = await pipe(pipe(mab, _.ap(ma))({}), bufferOB)
      assert.deepStrictEqual(x, [2])
    })

    it('chain', async () => {
      const f = (a: string) => _.of(a.length)

      const e1 = await pipe(pipe(_.of('foo'), _.chain(f))({}), bufferOB)
      assert.deepStrictEqual(e1, [3])
    })
  })

  it('ask', async () => {
    const e = await pipe(_.ask<number>()(1), bufferOB)
    return assert.deepStrictEqual(e, [1])
  })

  it('asks', async () => {
    const e = await pipe(_.asks((s: string) => s.length)('foo'), bufferOB)
    return assert.deepStrictEqual(e, [3])
  })

  it('local', async () => {
    const len = (s: string): number => s.length
    const e = await pipe(
      pipe(
        _.asks((n: number) => n + 1),
        _.local(len)
      )('aaa'),
      bufferOB
    )
    assert.deepStrictEqual(e, [4])
  })

  it('fromOption', async () => {
    const a = await pipe(_.fromOption(O.some(1))({}), bufferOB)
    assert.deepStrictEqual(a, [1])
  })

  it('fromTask', async () => {
    const e = await pipe(_.fromTask(T.of(1))({}), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('fromReaderTask', async () => {
    const e = await pipe(_.fromReaderTask(RT.of(1))({}), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('fromReader', async () => {
    const e = await pipe(_.fromReader(RE.of(1))({}), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('toReaderTask', async () => {
    const e = await _.toReaderTask(_.of(1))({})()
    assert.deepStrictEqual(e, 1)
  })

  it('toReaderTaskOption', async () => {
    const e1 = await _.toReaderTaskOption(_.of(1))({})()
    assert.deepStrictEqual(e1, O.some(1))
    const e2 = await _.toReaderTaskOption(_.of(undefined))({})()
    assert.deepStrictEqual(e2, O.some(undefined))
    const e3 = await _.toReaderTaskOption(_.zero())({})()
    assert.deepStrictEqual(e3, O.none)
  })

  it('filterMap', async () => {
    const fa = from([1, 2, 3])
    const fb = pipe(fa, R.filterMap(O.fromPredicate((n) => n > 1)))

    const e = await pipe(fb, bufferOB)

    assert.deepStrictEqual(e, [2, 3])
  })

  it('compact', async () => {
    const fa = () => from([1, 2, 3].map(O.fromPredicate((n) => n > 1)))
    const fb = _.compact(fa)

    const e = await pipe(fb({}), bufferOB)
    assert.deepStrictEqual(e, [2, 3])
  })

  it('filter', async () => {
    const fa = () => from([1, 2, 3])
    const fb = pipe(
      fa,
      _.filter((n) => n > 1)
    )

    const e = await pipe(fb({}), bufferOB)
    assert.deepStrictEqual(e, [2, 3])
  })

  it('partitionMap', async () => {
    const fa = () => from([1, 2, 3])
    const s = pipe(fa, _.partitionMap(E.fromPredicate((n) => n > 1, identity)))

    const e1 = await pipe(s.left({}), bufferOB)
    assert.deepStrictEqual(e1, [1])

    const e2 = await pipe(s.right({}), bufferOB)
    assert.deepStrictEqual(e2, [2, 3])
  })

  it('separate', async () => {
    const fa = () => from([1, 2, 3].map(E.fromPredicate((n) => n > 1, identity)))
    const s = _.separate(fa)

    const e1 = await pipe(s.left({}), bufferOB)
    assert.deepStrictEqual(e1, [1])

    const e2 = await pipe(s.right({}), bufferOB)
    assert.deepStrictEqual(e2, [2, 3])
  })

  it('partition', async () => {
    const fa = () => from([1, 2, 3])
    const s = pipe(
      fa,
      _.partition((n) => n > 1)
    )

    const e1 = await pipe(s.left({}), bufferOB)
    assert.deepStrictEqual(e1, [1])

    const e2 = await pipe(s.right({}), bufferOB)
    assert.deepStrictEqual(e2, [2, 3])
  })

  it('zero', async () => {
    const e = await pipe(_.zero()({}), bufferOB)
    assert.deepStrictEqual(e, [])
  })

  it('alt', async () => {
    const e = await pipe(
      pipe(
        _.of(1),
        _.alt(() => _.of(2))
      )({}),
      bufferOB
    )

    assert.deepStrictEqual(e, [1, 2])
  })

  it('getMonoid', async () => {
    const M = _.getMonoid()

    const e = await pipe(M.concat(_.of('a'), M.empty)({}), bufferOB)
    assert.deepStrictEqual(e, ['a'])

    const e2 = await pipe(M.concat(M.empty, _.of('b'))({}), bufferOB)
    assert.deepStrictEqual(e2, ['b'])

    const e3 = await pipe(M.concat(_.of('a'), _.of('b'))({}), bufferOB)
    assert.deepStrictEqual(e3, ['a', 'b'])
  })

  it('reader', async () => {
    const e = await pipe(_.fromReader(RE.of(1))({}), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('sequence parallel', async () => {
    const log: Array<string> = []
    const append = (message: string): _.ReaderObservable<{}, number> =>
      _.fromTask(() => Promise.resolve(log.push(message)))
    const t1 = pipe(
      append('start 1'),
      _.chain(() => append('end 1'))
    )
    const t2 = pipe(
      append('start 2'),
      _.chain(() => append('end 2'))
    )
    const sequenceParallel = A.sequence(_.Applicative)
    const ns = await pipe(sequenceParallel([t1, t2])({}), bufferOB)
    assert.deepStrictEqual(ns, [[3, 4]])
    assert.deepStrictEqual(log, ['start 1', 'start 2', 'end 1', 'end 2'])
  })

  describe('MonadIO', () => {
    it('fromIO', async () => {
      const e = await pipe(_.fromIO(() => 1)({}), bufferOB)

      assert.deepStrictEqual(e, [1])
    })
  })

  it('chainIOK', async () => {
    const f = (s: string) => I.of(s.length)
    const x = await _.run(pipe(_.of('a'), _.chainIOK(f)), undefined)
    assert.deepStrictEqual(x, 1)
  })

  it('chainTaskK', async () => {
    const f = (s: string) => R.of(s.length)
    const x = await _.run(pipe(_.of('a'), _.chainTaskK(f)), undefined)
    assert.deepStrictEqual(x, 1)
  })

  it('do notation', async () => {
    const t = await pipe(
      pipe(
        _.of(1),
        _.bindTo('a'),
        _.bind('b', () => _.of('b'))
      )(undefined),
      bufferOB
    )

    assert.deepStrictEqual(t, [{ a: 1, b: 'b' }])
  })

  it('apFirst', async () => {
    const e = await pipe(pipe(_.of(1), _.apFirst(_.of(2)))(undefined), bufferOB)
    assert.deepStrictEqual(e, [1])
  })

  it('apFirst', async () => {
    const e = await pipe(pipe(_.of(1), _.apSecond(_.of(2)))(undefined), bufferOB)
    assert.deepStrictEqual(e, [2])
  })

  it('chainFirst', async () => {
    const f = (a: string) => _.of(a.length)
    const e1 = await pipe(pipe(_.of('foo'), _.chainFirst(f))({}), bufferOB)
    assert.deepStrictEqual(e1, ['foo'])
  })
})
