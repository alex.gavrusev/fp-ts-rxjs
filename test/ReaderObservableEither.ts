import * as assert from 'assert'
import * as IO from 'fp-ts/IO'
import { flow, pipe } from 'fp-ts/function'
import * as O from 'fp-ts/Option'
import * as E from 'fp-ts/Either'
import * as R from 'fp-ts/Reader'
import * as T from 'fp-ts/Task'

import { observable as OB, observableEither as OBE, readerObservableEither as _ } from '../src'

import { bufferOB } from './test.helpers'

// test helper to dry up LOC.
export const buffer = R.map(flow(bufferOB, (p) => () => p))

describe('ReaderObservable', () => {
  it('map', async () => {
    const double = (n: number): number => n * 2

    const robe = pipe(_.of(3), _.map(double), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(6)])
  })

  it('ap', async () => {
    const double = (n: number): number => n * 2
    const mab = _.of(double)
    const ma = _.of(1)
    const robe = pipe(mab, _.ap(ma), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(2)])
  })

  it('chain', async () => {
    const f = (a: string) => _.of(a.length)
    const robe = pipe(_.of('foo'), _.chain(f), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(3)])
  })

  describe('bimap', () => {
    it('right', async () => {
      const double = (n: number): number => n * 2
      const doubleup = flow(double, double)

      const robe = pipe(_.of<unknown, number, number>(3), _.bimap(doubleup, double), buffer)
      const x = await robe({})()
      assert.deepStrictEqual(x, [E.right(6)])
    })

    it('left', async () => {
      const double = (n: number): number => n * 2
      const doubleup = flow(double, double)

      const robe = pipe(_.throwError<unknown, number, number>(3), _.bimap(doubleup, double), buffer)
      const x = await robe({})()
      assert.deepStrictEqual(x, [E.left(12)])
    })
  })

  it('mapLeft', async () => {
    const double = (n: number): number => n * 2
    const doubleup = flow(double, double)

    const robe = pipe(_.throwError<unknown, number, number>(3), _.mapLeft(doubleup), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.left(12)])
  })

  it('of', async () => {
    const robe = pipe(_.of('foo'), buffer)
    const x = await robe('')()
    assert.deepStrictEqual(x, [E.right('foo')])
  })

  it('ask', async () => {
    const robe = pipe(_.ask<string, any>(), buffer)
    const x = await robe('foo')()
    return assert.deepStrictEqual(x, [E.right('foo')])
  })

  it('asks', async () => {
    const robe = pipe(
      _.asks((s: string) => s.length),
      buffer
    )
    const x = await robe('foo')()
    return assert.deepStrictEqual(x, [E.right(3)])
  })

  it('local', async () => {
    const len = (s: string): number => s.length

    const robe = pipe(
      _.asks((n: number) => n + 1),
      _.local(len),
      buffer
    )
    const e = await robe('foo')()

    assert.deepStrictEqual(e, [E.right(4)])
  })

  it('fromTask', async () => {
    const robe = pipe(_.fromTask(T.of(1)), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  it('fromObservableEither', async () => {
    const robe = pipe(_.fromObservableEither(OBE.of(1)), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  it('fromReader', async () => {
    const robe = pipe(_.fromReader(R.of(1)), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  it('fromIO', async () => {
    const robe = pipe(_.fromIO(IO.of(1)), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  it('fromObservable', async () => {
    const robe = pipe(_.fromObservable(OB.of(1)), buffer)
    const x = await robe({})()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  // robe should expose right
  it('do notation', async () => {
    const t = await pipe(
      _.of(1),
      _.bindTo('a'),
      _.bind('b', () => _.of('b')),
      (f) => f(undefined),
      bufferOB
    )

    assert.deepStrictEqual(t, [E.right({ a: 1, b: 'b' })])
  })

  it('apFirst', async () => {
    const e = await pipe(_.of(1), _.apFirst(_.of(2)), (f) => f(undefined), bufferOB)

    assert.deepStrictEqual(e, [E.right(1)])
  })

  it('apFirst', async () => {
    const e = await pipe(_.of(1), _.apSecond(_.of(2)), (f) => f(undefined), bufferOB)

    assert.deepStrictEqual(e, [E.right(2)])
  })

  it('chainFirst', async () => {
    const f = (a: string) => _.of(a.length)

    const e1 = await pipe(_.of('foo'), _.chainFirst(f), (f) => f({}), bufferOB)
    assert.deepStrictEqual(e1, [E.right('foo')])
  })

  it('fromOption', async () => {
    const e1 = await pipe(
      O.some(1),
      _.fromOption(() => 'a'),
      (f) => f({}),
      bufferOB
    )

    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      O.none,
      _.fromOption(() => 'a'),
      (f) => f({}),
      bufferOB
    )

    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromEither', async () => {
    const e1 = await pipe(E.right(1), _.fromEither, (f) => f({}), bufferOB)
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(E.left('a'), _.fromEither, (f) => f({}), bufferOB)
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('filterOrElse', async () => {
    const e1 = await pipe(
      _.of(1),
      _.filterOrElse<unknown, number>(
        (n: number) => n > 0,
        () => 'a'
      ),
      (f) => f({}),
      bufferOB
    )
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      _.of(-1),
      _.filterOrElse<unknown, number>(
        (n: number) => n > 0,
        () => 'a'
      ),
      (f) => f({}),
      bufferOB
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromPredicate', async () => {
    const e1 = await pipe(
      1,
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      ),
      (f) => f(undefined),
      bufferOB
    )
    assert.deepStrictEqual(e1, [E.right(1)])

    const e2 = await pipe(
      -1,
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      ),
      (f) => f(undefined),
      bufferOB
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })
})
