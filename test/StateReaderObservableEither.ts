import * as assert from 'assert'
import * as E from 'fp-ts/Either'
import * as IO from 'fp-ts/IO'
import * as O from 'fp-ts/Option'
import { pipe } from 'fp-ts/function'
import * as T from 'fp-ts/Task'

import { observable as OB, stateReaderObservableEither as _ } from '../src'
import * as ROE from '../src/ReaderObservableEither'

import { buffer as bufferROE } from './ReaderObservableEither'
import { bufferOB } from './test.helpers'

function buffer<S, R, E, A>(
  srobe: _.StateReaderObservableEither<S, R, E, A>
): (s: S) => (r: R) => Promise<Array<E.Either<E, [A, S]>>> {
  return (s) => (r) => pipe(srobe(s)(r), bufferOB)
}

describe('stateReaderObservableEither', () => {
  test('right', async () => {
    const srobe = pipe(_.right<number, number, number, number>(3), buffer)
    const x = await srobe(1)(2)

    assert.deepStrictEqual(x, [E.right([3, 1])])
  })

  test('left', async () => {
    const srobe = pipe(_.left<number, number, number, number>(3), buffer)
    const x = await srobe(1)(2)

    assert.deepStrictEqual(x, [E.left(3)])
  })

  test('chain', async () => {
    const fa = _.right<number, number, number, number>(3)
    const fab = _.right<number, number, number, number>(6)
    const srobe = pipe(
      fa,
      _.chain(() => fab),
      buffer
    )
    const x = await srobe(1)(2)
    assert.deepStrictEqual(x, [E.right([6, 1])])
  })

  test('throwError', async () => {
    const srobe = pipe(_.throwError<number, number, number, number>(3), buffer)
    const x = await srobe(1)(2)

    assert.deepStrictEqual(x, [E.left(3)])
  })

  describe('Bifunctor', () => {
    const square = (a: number) => a * a
    const doublesquare = (a: number) => a ** a
    const bimap = _.bimap(doublesquare, square)

    test('map', async () => {
      const srobe = pipe(_.right<number, number, number, number>(3), bimap, buffer)
      const x = await srobe(1)(2)

      assert.deepStrictEqual(x, [E.right([9, 1])])
    })

    test('mapLeft', async () => {
      const srobe = pipe(_.left<number, number, number, number>(3), bimap, buffer)
      const x = await srobe(1)(2)

      assert.deepStrictEqual(x, [E.left(27)])
    })
  })

  test('fromIO', async () => {
    const srobe = pipe(_.fromIO(IO.of(3)), buffer)
    const x = await srobe(1)(2)
    assert.deepStrictEqual(x, [E.right([3, 1])])
  })

  test('fromTask', async () => {
    const srobe = pipe(_.fromTask(T.of(3)), buffer)
    const x = await srobe(1)(2)
    assert.deepStrictEqual(x, [E.right([3, 1])])
  })

  test('fromObservable', async () => {
    const srobe = pipe(_.fromObservable(OB.of(3)), buffer)
    const x = await srobe(1)(2)
    assert.deepStrictEqual(x, [E.right([3, 1])])
  })

  test('evaluate', async () => {
    const srobe = pipe(_.right<number, number, number, number>(3), _.evaluate(1), bufferROE)
    const x = await srobe(2)()
    assert.deepStrictEqual(x, [E.right(3)])
  })

  test('execute', async () => {
    const srobe = pipe(_.right<number, number, number, number>(3), _.execute(1), bufferROE)
    const x = await srobe(2)()
    assert.deepStrictEqual(x, [E.right(1)])
  })

  // should expose of
  it('do notation', async () => {
    const srobe = pipe(
      _.right(1),
      _.bindTo('a'),
      _.bind('b', () => _.right('b')),
      buffer
    )
    const x = await srobe('state')('reader')
    assert.deepStrictEqual(x, [E.right([{ a: 1, b: 'b' }, 'state'])])
  })

  it('apFirst', async () => {
    const e = await pipe(_.of(1), _.apFirst(_.of(2)), buffer, (f) => f(undefined)({}))

    assert.deepStrictEqual(e, [E.right([1, undefined])])
  })

  it('apSecond', async () => {
    const e = await pipe(_.of(1), _.apSecond(_.of(2)), buffer, (f) => f(undefined)({}))

    assert.deepStrictEqual(e, [E.right([2, undefined])])
  })

  it('chainFirst', async () => {
    const f = (a: string) => _.of(a.length)
    const e1 = await pipe(_.of('foo'), _.chainFirst(f), buffer, (f) => f({})({}))
    assert.deepStrictEqual(e1, [E.right(['foo', {}])])
  })

  it('fromOption', async () => {
    const e1 = await pipe(
      O.some(1),
      _.fromOption(() => 'a'),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e1, [E.right([1, {}])])

    const e2 = await pipe(
      O.none,
      _.fromOption(() => 'a'),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromEither', async () => {
    const e1 = await pipe(E.right(1), _.fromEither, buffer, (f) => f({})({}))
    assert.deepStrictEqual(e1, [E.right([1, {}])])

    const e2 = await pipe(E.left('a'), _.fromEither, buffer, (f) => f({})({}))
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('filterOrElse', async () => {
    const e1 = await pipe(
      _.of<unknown, unknown, string, number>(1),
      _.filterOrElse(
        (n: number) => n > 0,
        () => 'a'
      ),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e1, [E.right([1, {}])])

    const e2 = await pipe(
      _.of<unknown, unknown, string, number>(-1),
      _.filterOrElse(
        (n: number) => n > 0,
        () => 'a'
      ),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromPredicate', async () => {
    const e1 = await pipe(
      1,
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      ),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e1, [E.right([1, {}])])

    const e2 = await pipe(
      -1,
      _.fromPredicate(
        (n: number) => n > 0,
        () => 'a'
      ),
      buffer,
      (f) => f({})({})
    )
    assert.deepStrictEqual(e2, [E.left('a')])
  })

  it('fromReaderObservableEither', async () => {
    const e = await pipe(ROE.right(1), _.fromReaderObservableEither, buffer, (f) => f({})({}))
    assert.deepStrictEqual(e, [E.right([1, {}])])
  })

  it('get', async () => {
    const e = await pipe(_.get(), buffer, (f) => f(1)(undefined))
    assert.deepStrictEqual(e, [E.right([1, 1])])
  })

  it('gets', async () => {
    const e = await pipe(
      (n: number) => n * 2,
      _.gets,
      buffer,
      (f) => f(1)(undefined)
    )
    assert.deepStrictEqual(e, [E.right([2, 1])])
  })

  it('modify', async () => {
    const e = await pipe(
      (n: number) => n * 2,
      _.modify,
      buffer,
      (f) => f(1)(undefined)
    )
    assert.deepStrictEqual(e, [E.right([undefined, 2])])
  })

  it('put', async () => {
    const e = await pipe(2, _.put, buffer, (f) => f(1)(undefined))
    assert.deepStrictEqual(e, [E.right([undefined, 2])])
  })
})
