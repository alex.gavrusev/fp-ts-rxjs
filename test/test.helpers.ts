import { pipe } from 'fp-ts/function'
import { bufferTime, lastValueFrom, Observable } from 'rxjs'

export const bufferOB = <A>(o: Observable<A>): Promise<Array<A>> => pipe(o, bufferTime(10), (o) => lastValueFrom(o))
